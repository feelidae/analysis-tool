package analysistool;

import static org.junit.Assert.*;
import java.io.File;

import org.junit.Test;

public class AnalysisToolTest {

	@Test
	public void testAnalysisTool() {
	    File sourceFile = new File("src/test/resources/BasicTest.txt");
	    File destinationFile = new File("src/test/resources/Analysis.txt");
	    destinationFile.delete();
	    AnalysisTool.main(new String[] {sourceFile.getAbsolutePath().toString(), destinationFile.getAbsolutePath().toString()});
	    
	    assert(destinationFile.exists());
	    // more unit tests...
	}

}
