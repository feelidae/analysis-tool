package analysistool;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class LogDataTest {

    @Test
    public void testInsertProfilingEvent() {
        LogData ld = new LogData();
        assertEquals(0, ld.getNumberOfDurations("Test"));
        ld.insertProfilingevent("Test", 3);
        assertEquals(1, ld.getNumberOfDurations("Test"));
        assertEquals(3, ld.getFirstDuration("Test"));
    }
    
    @Test
    public void testCalculateDuration() {
        LogData ld = new LogData();
        ld.insertProfilingevent("Test", 34);
        ld.insertProfilingevent("Test", 52);
        ld.insertProfilingevent("Test", 21);
        ld.insertProfilingevent("Test", 105);
        ld.insertProfilingevent("Test", 55);
        ld.insertProfilingevent("Test2", 200);
        ld.insertProfilingevent("Test", 68);
        ld.insertProfilingevent("Test", 224);
        ld.insertProfilingevent("Test", 17);
        ld.insertProfilingevent("Test", 120);
        ld.insertProfilingevent("Test2", 1);
        LinkedList<String> eventnames = ld.getEventnames();
        Collections.sort(eventnames);
        
        assertEquals(9, ld.getNumberOfDurations("Test"));
        assertEquals(2, ld.getNumberOfDurations("Test2"));
        assertEquals(Math.round(77.33), ld.getAverageDuration("Test"));
        assertEquals(Math.round(100.5), ld.getAverageDuration("Test2"), 0.1);
        assertEquals(17, ld.getMinimumDuration("Test"));
        assertEquals(1, ld.getMinimumDuration("Test2"));
        assertEquals(224, ld.getMaximumDuration("Test"));
        assertEquals(200, ld.getMaximumDuration("Test2"));
        assertEquals(34, ld.getFirstDuration("Test"));
        assertEquals(200, ld.getFirstDuration("Test2"));        
        assertEquals(120, ld.getLastDuration("Test"));
        assertEquals(1, ld.getLastDuration("Test2"));
        assertEquals(2, eventnames.size());
        assertEquals(Arrays.asList("Test", "Test2"), eventnames);
        
        // more unit tests...
    }

}
