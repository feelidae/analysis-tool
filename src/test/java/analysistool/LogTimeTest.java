package analysistool;

import static org.junit.Assert.*;
import org.junit.Test;

public class LogTimeTest {

    @Test
    public void test() {
        LogTime lt1 = new LogTime(12,30,10,100);
        LogTime lt2 = new LogTime(12,30,10,830);
        LogTime lt3 = new LogTime(12,30,11,50);
        LogTime lt4 = new LogTime(23,59,59,999);
        LogTime lt5 = new LogTime(0,0,0,1);        
        
        assertEquals(12, lt1.getHour());
        assertEquals(30, lt1.getMinute());
        assertEquals(10, lt1.getSecond());
        assertEquals(100, lt1.getMillisecond());
        
        assertEquals(0, lt1.getTimeDifferenceInMilliseconds(lt1));  
        assertEquals(730, lt2.getTimeDifferenceInMilliseconds(lt1));
        // if a measurement starts at 12:30:11 and ends at 12:30:10 the tool assumes (almost) an entire day has passed
        assertEquals(86399270, lt1.getTimeDifferenceInMilliseconds(lt2));
        assertEquals(950, lt3.getTimeDifferenceInMilliseconds(lt1)); 
        assertEquals(2, lt5.getTimeDifferenceInMilliseconds(lt4));       
    }

}
