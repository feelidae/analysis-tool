package analysistool;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class FileParserTest {
	
	private FileParser fp;
	
	@Before
	public void initializeFileParser() {
		fp = new FileParser();
	}

	@Test
	public void testParseFileBasic() {
	    LogData data = fp.parseFile(new File("src/test/resources/BasicTest.txt").getAbsolutePath().toString());
	    LinkedList<String> eventnames = data.getEventnames();
	    Collections.sort(eventnames);
	    
	    assertEquals(5, eventnames.size());
	    assertEquals(Arrays.asList("DEVICE_TEST", "DEVICE_TEST.SUBTEST \"check connection\"", "DEVICE_TEST.SUBTEST \"measure leakage current\"", "INSERTION", "REMOVAL"), eventnames);
        // more unit tests...    
	}
	
   @Test
    public void testParseFileMidnight() {
        LogData data = fp.parseFile(new File("src/test/resources/MidnightTest.txt").getAbsolutePath().toString());
        LinkedList<String> eventnames = data.getEventnames();
        Collections.sort(eventnames);
        
        assertEquals(Arrays.asList("DEVICE_TEST", "DEVICE_TEST.SUBTEST \"check connection\"", "DEVICE_TEST.SUBTEST \"measure leakage current\"", "DEVICE_TEST.SUBTEST \"test logic\"", "INSERTION", "REMOVAL"), eventnames);
        assertEquals(61000, data.getFirstDuration("INSERTION"));
        // more unit tests...    
    }
   
   @Test
   public void testParseFileMultiple() {
       LogData data = fp.parseFile(new File("src/test/resources/MultipledevicesTest.txt").getAbsolutePath().toString());
       LinkedList<String> eventnames = data.getEventnames();
       Collections.sort(eventnames);
       
       assertEquals(3, data.getNumberOfDurations("INSERTION"));
       assertEquals(2100, data.getFirstDuration("INSERTION"));
       assertEquals(1700, data.getLastDuration("INSERTION"));
       assertEquals(62100, data.getMaximumDuration("INSERTION"));
       assertEquals(1700, data.getMinimumDuration("INSERTION"));
       assertEquals(21967, data.getAverageDuration("INSERTION"));       
       // more unit tests...    
   }
}
