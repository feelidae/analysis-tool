package analysistool;

/**
* <b>Log Time</b>
* This class saves a point in time indicated by hour, minute,
* second and millisecond at which an event took place. It can also
* calculate the difference between its own time and another {@link LogTime}.
* @author  Sandra Thiemermann
* @version 0.1
*/
public class LogTime {
	
	private int hour;
	private int minute;
	private int second;
	private int millisecond;

    /**
     * Creates a new {@link LogTime} to save a point in time.
     * @param hour
     * @param minute 
     * @param second
     * @param millisecond
     */ 	
	public LogTime(int hour, int minute, int second, int millisecond) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		this.millisecond = millisecond;
	}
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public int getSecond() {
		return second;
	}
	
	public int getMillisecond() {
		return millisecond;
	}
	
	/**
	 * Calculates the time difference between this point in time and a previous point in time.
	 * @param  previous time		a previous point in time 
	 * @return 						duration between these two points in time in ms
	 */
	public int getTimeDifferenceInMilliseconds(LogTime previousTime) {
		int timeDifference =    (millisecond-previousTime.getMillisecond()+
				 				(second-previousTime.getSecond())*1000+
				 				(minute-previousTime.getMinute())*60000+
				 				(hour-previousTime.getHour())*3600000);
		/* If this difference is negative our current time seems to be *before* the previous time.
		 * In this case we assume a new day has begun and therefore add 86400000 milliseconds. This
		 * ensures measurements which go past midnight won't cause issues. */
		if(timeDifference < 0) {
			// adding one day in milliseconds
			timeDifference += 86400000;
		}
		return timeDifference;
	}
}
