package analysistool;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Iterator;

/**
* <b>Analysis Tool for Log Files</b>
* The AnalysisTool provides a simple program
* to analyze log files for an automated production line.
*
* @author  Sandra Thiemermann
* @version 0.1
*/
public class AnalysisTool {
	private static FileParser fileParser;
	private static LogData profilingEvents;
	private static String sourcePath;
	private static String destinationPath;
	
	
	/**
	 * Accepts paths to a source log file and a destination file respectively via the command
	 * line interface, performs an analysis on the source file without changing
	 * its contents and saves the result to the destination file.
	 * @param  args			path to source file, path to destination file     
	 */
	public static void main(String[] args) {
		if(args.length < 2) {
			System.out.println("Please state paths to log file and a destination file respectively to save the result.");
			System.exit(0);
		}
		
		sourcePath = args[0];
		destinationPath = args[1];
		fileParser = new FileParser();
		profilingEvents = fileParser.parseFile(sourcePath);
		List<String> eventnames = profilingEvents.getEventnames();
		
		try {
			BufferedWriter filewriter = new BufferedWriter(new FileWriter(destinationPath));
			Iterator<String> eventIterator = eventnames.iterator();
			while(eventIterator.hasNext()) {
				String currentEvent = eventIterator.next();
				int currentNumberOfMeasurements = profilingEvents.getNumberOfDurations(currentEvent);
				filewriter.write(String.format("%s (# of measurements: %d)", currentEvent, currentNumberOfMeasurements));
				filewriter.newLine();
				int currentMin = profilingEvents.getMinimumDuration(currentEvent);
				int currentMax = profilingEvents.getMaximumDuration(currentEvent);
				int currentAverage = profilingEvents.getAverageDuration(currentEvent);
				int currentFirst = profilingEvents.getFirstDuration(currentEvent);
				int currentLast = profilingEvents.getLastDuration(currentEvent);
				filewriter.write(String.format("%-20s %d ms%n"
							                  +"%-20s %d ms%n"
										      +"%-20s %d ms%n"
										      +"%-20s %d ms%n"
										      +"%-20s %d ms%n"
										      +"----------------%n",
										      "AVG:", currentAverage,
										      "MIN:", currentMin,
										      "MAX:", currentMax,
										      "FIRST:", currentFirst,
										      "LAST:", currentLast));
			}
			filewriter.close();
		} catch(IOException e) {
			System.out.println("An error occured and the result could not be saved to file. "+e.getMessage());
			System.exit(0);
		}
		System.out.println("Analysis complete. Result can be found in "+destinationPath);
	}	
}