package analysistool;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import analysistool.LogTime;

/**
* <b>File Parser for Log Files</b>
* This class can parse files that follow this format (per line):<br>
* #PE[HH:mm:ss.SSS] : BEGIN|END TEST "OPTIONAL TEST NAME"<br>
* <i>Example: #PE[14:37:31.300] : BEGIN INSERTION "device #13"
*
* @author  Sandra Thiemermann
* @version 0.1
*/
public class FileParser {
	private HashMap<String, LogTime> startedEvents;
	String activeSubtest;
	
    /**
     * Creates a new {@link FileParser} to parse log files.     
     */ 
    public FileParser() {
        startedEvents = new HashMap<String, LogTime>();
    }
	
	/**
	 * Returns a {@link LogTime} object containing the time found in the 
	 * argument string. The argument string must follow the
	 * specified format: "HH:mm:ss.SSS"
	 * @param  dateString	the string containing the date
	 * @return				a {@link LogTime} object containing the parsed time       
	 */
	private LogTime parseDate(String dateString) {
		int hour = Integer.valueOf(dateString.substring(0,2));
		int minute = Integer.valueOf(dateString.substring(3,5));
		int second = Integer.valueOf(dateString.substring(6,8));
		int millisecond = Integer.valueOf(dateString.substring(9,12));		
		return new LogTime(hour, minute, second, millisecond);
	}
	
	/**
	 * Parses a given log file and saves the durations of all detected
	 * profiling events (insertion, device_test, all device test subtests
	 * and removal) to a {@link LogData} object
	 * @param  filepath  	a path to the log file
	 * @return				a {@link LogData} object containing the parsed profiling events       
	 */
	public LogData parseFile(String filepath) {
		LogData fileData = new LogData();		
		try(BufferedReader fileReader = new BufferedReader(new FileReader(filepath))) {
			String line;
			while((line = fileReader.readLine()) != null) {
				LogTime eventTime = parseDate(line.substring(4,16));
				
				// split the remaining line on whitespace " " to extract action and event name
				String[] lineParts = line.substring(20).split(" ");
				String action = lineParts[0]; // "BEGIN" or "END"
				String event = lineParts[1]; // "REMOVAL", "INSERTION"...
				if(event.equals("DEVICE_TEST.SUBTEST") && action.equals("BEGIN")) {
					activeSubtest = line.substring(22+action.length()+event.length());
				}
				
				if(action.equals("BEGIN")) {
					startedEvents.put(event, eventTime);
				} else {
					LogTime startTime = startedEvents.remove(event);
					int durationInMilliseconds = eventTime.getTimeDifferenceInMilliseconds(startTime);
					String eventname = event;
					if(event.equals("DEVICE_TEST.SUBTEST")) {
						// if it's a device test subtest, save the name of the active subtest as part of the event name
						eventname += " "+activeSubtest;
					}
					fileData.insertProfilingevent(eventname, durationInMilliseconds);					
				}

			}
		} catch(IOException e) {
			System.out.println("The given file could not be parsed. "+e.getMessage());
			System.exit(0);
		}
		
		return fileData;
	}
}