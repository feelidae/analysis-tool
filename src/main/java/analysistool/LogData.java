package analysistool;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Iterator;

/**
* <b>Log Data</b>
* This class saves the names and durations of profiling events
* and performs calculations on it.
*
* @author  Sandra Thiemermann
* @version 0.1
*/
public class LogData {
	HashMap<String, LinkedList<Integer>> profilingeventsDurations;
	
    /**
     * Creates a new {@link LogData} to save the names and durations profiling events
     * and perform calculations on them.     
     */ 
	public LogData() {
		profilingeventsDurations = new HashMap<String, LinkedList<Integer>>();
	}
	
	/**
	 * Saves new events.
	 * @param  eventName			name of the event 
	 * @param eventDuration			duration of the event in ms
	 */
	public void insertProfilingevent(String eventName, int eventDuration) {
		profilingeventsDurations.putIfAbsent(eventName, new LinkedList<Integer>());
		profilingeventsDurations.get(eventName).add(eventDuration);
	}
	
	/**
	 * Returns the minimum duration for a given event type in milliseconds.
	 * @param  eventName			name of the event 
	 * @return 						minimum duration in ms
	 */	
	public int getMinimumDuration(String eventName) {
		return Collections.min(profilingeventsDurations.getOrDefault(eventName, new LinkedList<Integer>()));
	}
	
	/**
	 * Returns the maximum duration for a given event type in milliseconds.
	 * @param  eventName			name of the event 
	 * @return 						maximum duration in ms
	 */	
	public int getMaximumDuration(String eventName) {
		return Collections.max(profilingeventsDurations.getOrDefault(eventName, new LinkedList<Integer>()));
	}
	
	/**
	 * Returns the average duration for a given event type in milliseconds.
	 * @param  eventName			name of the event 
	 * @return 						average duration in ms
	 */	
	public int getAverageDuration(String eventName) {
		float mean = 0;
		LinkedList<Integer> durations = profilingeventsDurations.get(eventName);
		int numberEvents = durations.size();
		Iterator<Integer> durationIterator = durations.iterator();
		while(durationIterator.hasNext()) {
			mean += (float)durationIterator.next()/numberEvents;
		}
		return Math.round(mean);
	}
	
	/**
	 * Returns the duration of the first measurement for a given event type in milliseconds.
	 * @param  eventName			name of the event 
	 * @return 						duration of first measurement in ms
	 */	
	public int getFirstDuration(String eventName) {
		return profilingeventsDurations.getOrDefault(eventName, new LinkedList<Integer>()).getFirst();
	}
	
	/**
	 * Returns the duration of the last measurement for a given event type in milliseconds.
	 * @param  eventName			name of the event 
	 * @return 						duration of last measurement in ms
	 */
	public int getLastDuration(String eventName) {
		return profilingeventsDurations.getOrDefault(eventName, new LinkedList<Integer>()).getLast();
	}
	
	/**
	 * Returns how many measurements exist for a given event type.
	 * @param  eventName			name of the event 
	 * @return 						duration the number of measurements
	 */
	public int getNumberOfDurations(String eventName) {
		return profilingeventsDurations.getOrDefault(eventName, new LinkedList<Integer>()).size();
	}
	
	/**
	 * Returns a list of all event types stored in this data object.
	 * @return 						a list of all event types
	 */	
	public LinkedList<String> getEventnames() {
		return new LinkedList<String>(profilingeventsDurations.keySet());
	}
}